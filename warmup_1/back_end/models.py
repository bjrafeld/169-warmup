from django.db import models

class User(models.Model):
    user = models.CharField(max_length=128)
    password = models.CharField(max_length=128)
    count = models.PositiveIntegerField()
    
    @classmethod
    def create(cls, u, p):
        user = cls(user=u,password=p,count=1)
        return user
    
    def __unicode__(self):
        return self.user


SUCCESS                 =   1   #login success
ERR_BAD_CREDENTIALS     =   -1  # (for login) cannot find the user/password pair in the db
ERR_USER_EXISTS         =   -2  #(for add) trying to add the user that already exists
ERR_BAD_USERNAME        =   -3  #(for add) invalid user name (user name should be non-empty and at most 128 ascii characters long)
ERR_BAD_PASSWORD        =   -4  #(for add) invalid password (password should be at most 128 ascii characters)

class UsersModels():
    
    @staticmethod
    def login(username, password):
        result={}
        try:
            u = User.objects.get(user=username)
        except User.DoesNotExist:
            return ERR_BAD_CREDENTIALS
        else:
            p = u.password
            if p==password:
                u.count+=1
                u.save()
                return u.count
            else:
                return ERR_BAD_CREDENTIALS
        return result
    
    @staticmethod
    def add(username, password):
        result={}
        try:
            u = User.objects.get(user=username)
        except User.DoesNotExist:
            if username=="" or username.__len__()>128:
                return ERR_BAD_USERNAME
            elif password=="" or password.__len__()>128:
                return ERR_BAD_PASSWORD
            else:
                new_user = User.create(username,password)
                new_user.save()
                return new_user.count
        else:
            return ERR_USER_EXISTS
    
    @staticmethod
    def TESTAPI_resetFixture():
        User.objects.all().delete()
        return 1


class UnitTests():
    
    def __init__(self):    
        self.total_tests = 0
        self.tests_failed = 0
    
    def setUp(self):
        UsersModels.TESTAPI_resetFixture()
    
    
    def testAdd1(self):
        self.setUp()
        self.total_tests+=1
        result = UsersModels.add('user','pass')
        if result != SUCCESS:
            self.tests_failed+=1
            return 'testAdd1...FAIL'
        else:
            return 'testAdd1...OK'
    
    def testAddExists(self):
        self.setUp()
        self.total_tests+=1
        UsersModels.add('user','pass')
        result = UsersModels.add('user','pass')
        if result != ERR_USER_EXISTS:
            self.tests_failed+=1
            return 'testAddExists...FAIL'
        else:
            return 'testAddExists...OK'
    
    def testAdd2(self):
        self.setUp()
        self.total_tests+=1
        result = UsersModels.add('user','pass')
        result2 = UsersModels.add('user1','pass')
        if result != SUCCESS or result2 !=SUCCESS:
            self.tests_failed+=1
            return 'testAdd2...FAIL'
        else:
            return 'testAdd2...OK'
    
    def testAddEmptyUser(self):
        self.setUp()
        self.total_tests+=1
        result = UsersModels.add('','pass')
        if result != ERR_BAD_USERNAME:
            self.tests_failed+=1
            return 'testAddEmptyUser...FAIL'
        else:
            return 'testAddWrongUser...OK'
    
    def testAddLongUser(self):
        self.setUp()
        self.total_tests+=1
        result = UsersModels.add('hghghghghghghghghhkhkfhaeifhiaehfoihefoiaheoifhoaihfoiaheiofhoaidhfioahdoifhaoihfioadhoifhoaidhfoahdfoiahdoifhadoifhaoidhfoiadhfoiadhfoiadhfoiadhfoiahdfoiahdfoiahdoifaho','pass')
        if result != ERR_BAD_USERNAME:
            self.tests_failed+=1
            return 'testAddLongUser...FAIL'
        else:
            return 'testAddLongUser...OK'
    
    def testAddEmptyPass(self):
        self.setUp()
        self.total_tests+=1
        result = UsersModels.add('user','')
        if result != ERR_BAD_PASSWORD:
            self.tests_failed+=1
            return 'testAddEmptyPass...FAIL'
        else:
            return 'testAddEmptyPass...OK'
    
    def testAddLongPass(self):
        self.setUp()
        self.total_tests+=1
        result = UsersModels.add('user1','hghghghghghghghghhkhkfhaeifhiaehfoihefoiaheoifhoaihfoiaheiofhoaidhfioahdoifhaoihfioadhoifhoaidhfoahdfoiahdoifhadoifhaoidhfoiadhfoiadhfoiadhfoiadhfoiahdfoiahdfoiahdoifaho')
        if result != ERR_BAD_PASSWORD:
            self.tests_failed+=1
            return 'testAddLongPass...FAIL'
        else:
            return 'testAddLongPass...OK'
    
    def testLoginTwice(self):
        self.setUp()
        self.total_tests+=1
        UsersModels.add('user','pass')
        result = UsersModels.login('user','pass')
        if result != 2:     #count=2 for second login
            self.tests_failed+=1
            return 'testLogin2...FAIL'
        else:
            return 'testLogin2...OK'
    
    def testLoginThrice(self):
        self.setUp()
        self.total_tests+=1
        UsersModels.add('user','pass')
        UsersModels.login('user','pass')
        result = UsersModels.login('user','pass')
        if result != 3:     #count=2 for second login
            self.tests_failed+=1
            return 'testLogin3...FAIL'
        else:
            return 'testLogin3...OK'
    
    def testLoginWrong(self):
        self.setUp()
        self.total_tests+=1
        UsersModels.add('user','pass')
        result = UsersModels.login('user1','pass1')
        if result != ERR_BAD_CREDENTIALS:
            self.tests_failed+=1
            return 'testLoginWrong...FAIL'
        else:
            return 'testLoginWrong...OK'
    
    def testUserDoesNotExist(self):
        self.setUp()
        self.total_tests+=1
        result = UsersModels.login('user1','pass')
        if result != ERR_BAD_CREDENTIALS:
            self.tests_failed+=1
            return 'testUserDoesNotExist...FAIL'
        else:
            return 'testUserDoesNotExist...OK'
    
    
    def runTests(self):
        output = ('\n'+self.testAdd1() +'\n')
        output += (self.testAddExists() +'\n')
        output += (self.testAdd2() +'\n')
        output += (self.testAddEmptyUser() +'\n')
        output += (self.testAddEmptyPass() +'\n')
        output += (self.testAddLongUser() +'\n')
        output += (self.testAddLongPass() +'\n')
        output += (self.testLoginTwice() +'\n')
        output += (self.testLoginThrice() +'\n')
        output += (self.testLoginWrong() +'\n')
        output += (self.testUserDoesNotExist() +'\n')
        self.setUp()
        return output

