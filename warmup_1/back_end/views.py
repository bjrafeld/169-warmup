from django.shortcuts import render_to_response, render
from django.http import HttpResponse, HttpResponseServerError
from django.views.decorators.csrf import csrf_exempt
from models import User, UsersModels, UnitTests
from django.template import Template, Context, loader
import json
import sys, traceback

@csrf_exempt
def ui_view(request):
    t = loader.get_template('login.html')
    return HttpResponse(t.render(Context({})));
        

@csrf_exempt
def login_view(request):
    info = json.loads(request.body)
    u = info['user']
    p = info['password']
    if request.method == 'POST':
        result = UsersModels.login(u,p)
        result_info = {}
        if result < 0:
            result_info['errCode'] = result;
        else:
            result_info['errCode']=1
            result_info['count']=result
        return HttpResponse(json.dumps(result_info), mimetype='application/json')

@csrf_exempt
def add_view(request):
    info = json.loads(request.body)
    u=info['user']
    p=info['password']
    if request.method == 'POST':
        result = UsersModels.add(u,p)
        result_info = {}
        if result < 0:
            result_info['errCode'] = result;
        else:
            result_info['errCode']=1
            result_info['count']=result
        return HttpResponse(json.dumps(result_info),mimetype='application/json')

@csrf_exempt
def reset_view(request):
    if request.method == 'POST':
        result_info={}
        result_info['errcode']=1
        UsersModels.TESTAPI_resetFixture()
        return HttpResponse(json.dumps(result_info), mimetype='application/json')

@csrf_exempt
def unit_tests(request):
    if request.method == 'POST':
        result_info={}
        tests = UnitTests()
        result = tests.runTests();
        result_info['totalTests']=tests.total_tests
        result_info['nrFailed']=tests.tests_failed
        result_info['output'] = result
        return HttpResponse(json.dumps(result_info), mimetype='application/json')

def custom_500(request):
    t = loader.get_template('500.html')
    type, value, tb = sys.exc_info()
    #traceback.print_exc(tb)
    return HttpResponseServerError(t.render(Context({'exception_value': value,})))
