from django.conf.urls import patterns, include, url
from django.conf import settings
from back_end.views import login_view, add_view, reset_view, unit_tests, custom_500, ui_view

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'mysite.views.home', name='home'),
                       # url(r'^mysite/', include('mysite.foo.urls')),
                       
                       # Uncomment the admin/doc line below to enable admin documentation:
                       # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
                       
                       # Uncomment the next line to enable the admin:
                       # url(r'^admin/', include(admin.site.urls)),
                       (r'^$', ui_view),
                       (r'^users/login$',login_view),
                       (r'^users/add$',add_view),
                       (r'^TESTAPI/resetFixture$',reset_view),
                       (r'^TESTAPI/unitTests$', unit_tests),
                       (r'^static/(?P<path>.*)$', 'django.views.static.serve',
                        {'document_root': settings.MEDIA_ROOT}),
                       )
handler500 = custom_500
