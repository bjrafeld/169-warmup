import unittest
import os
import testLib




class TestLoginUser(testLib.RestTestCase):
    """Test Logging in"""

    def assertResponse(self, respData, count = 1, errCode = testLib.RestTestCase.SUCCESS):
        expected = { 'errCode' : errCode}
        if count is not None:
            expected['count'] = count
        self.assertDictEqual(expected, respData)
    
    def testLogin(self):
        self.makeRequest("/users/add", method="POST", data = { 'user' : 'user1', 'password' : 'password'} )
        respData = self.makeRequest("/users/login", method="POST", data = { 'user' : 'user1', 'password' : 'password'} )
        self.assertResponse(respData, count=2)

class TestAddWrongUser(testLib.RestTestCase):
    """Test Logging in with Empty Username"""
    
    def assertResponse(self, respData, count = 1, errCode = testLib.RestTestCase.SUCCESS):
        expected = { 'errCode' : errCode}
        if count is not None:
            expected['count'] = count
        self.assertDictEqual(expected, respData)
    
    def testWrongUser(self):
        respData = self.makeRequest("/users/add", method="POST", data = { 'user' : '', 'password' : 'password'} )
        self.assertResponse(respData, errCode=testLib.RestTestCase.ERR_BAD_USERNAME, count = None)

class TestAddWrongPass(testLib.RestTestCase):
    """Test Logging in with Empty Password"""
    
    def assertResponse(self, respData, count = 1, errCode = testLib.RestTestCase.SUCCESS):
        expected = { 'errCode' : errCode}
        if count is not None:
            expected['count'] = count
        self.assertDictEqual(expected, respData)
    
    def testWrongPass(self):
        respData = self.makeRequest("/users/add", method="POST", data = { 'user' : 'user1', 'password' : ''} )
        self.assertResponse(respData, errCode=testLib.RestTestCase.ERR_BAD_PASSWORD, count = None)

class TestAddUserExisting(testLib.RestTestCase):
    """Test Logging in with Empty Username"""
    
    def assertResponse(self, respData, count = 1, errCode = testLib.RestTestCase.SUCCESS):
        expected = { 'errCode' : errCode}
        if count is not None:
            expected['count'] = count
        self.assertDictEqual(expected, respData)
    
    def testLoginExisting(self):
        self.makeRequest("/users/add", method="POST", data = { 'user' : 'user1', 'password' : 'password'} )
        respData = self.makeRequest("/users/add", method="POST", data = { 'user' : 'user1', 'password' : 'password'} )
        self.assertResponse(respData, errCode=testLib.RestTestCase.ERR_USER_EXISTS, count = None)

class TestLoginWrongUser(testLib.RestTestCase):
    """Test Logging in"""
    
    def assertResponse(self, respData, count = 1, errCode = testLib.RestTestCase.SUCCESS):
        expected = { 'errCode' : errCode}
        if count is not None:
            expected['count'] = count
        self.assertDictEqual(expected, respData)
    
    def testLogin(self):
        self.makeRequest("/users/add", method="POST", data = { 'user' : 'user1', 'password' : 'password'} )
        respData = self.makeRequest("/users/login", method="POST", data = { 'user' : 'user', 'password' : 'password'} )
        self.assertResponse(respData, count=None, errCode = testLib.RestTestCase.ERR_BAD_CREDENTIALS)

class TestLoginUserWrongPass(testLib.RestTestCase):
    """Test Logging in"""
    
    def assertResponse(self, respData, count = 1, errCode = testLib.RestTestCase.SUCCESS):
        expected = { 'errCode' : errCode}
        if count is not None:
            expected['count'] = count
        self.assertDictEqual(expected, respData)
    
    def testLogin(self):
        self.makeRequest("/users/add", method="POST", data = { 'user' : 'user1', 'password' : 'password'} )
        respData = self.makeRequest("/users/login", method="POST", data = { 'user' : 'user1', 'password' : 'pass'} )
        self.assertResponse(respData, count=None, errCode = testLib.RestTestCase.ERR_BAD_CREDENTIALS)

